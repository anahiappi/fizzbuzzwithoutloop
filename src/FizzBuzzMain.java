import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class FizzBuzzMain {
	public static void main(String[] args) {

		// {\x->"Fizz", id, id}
		Function<String,String>[] fizz
		= Stream.generate(() -> Function.identity()).limit(3).toArray(Function[]::new);
		fizz[0] = s -> "Fizz";
		// {\x->"Buzz", id, id, id, id}
		Function<String,String>[] buzz
		= Stream.generate(() -> Function.identity()).limit(5).toArray(Function[]::new);
		buzz[0] = s -> "Buzz";
		// {\x->"FizzBuzz", id, id, ..., id}
		Function<String,String>[] fizzbuzz
		= Stream.generate(() -> Function.identity()).limit(15).toArray(Function[]::new);
		fizzbuzz[0] = s -> "FizzBuzz";

		//1～30の整数に、上のラムダ式を適用
		IntStream.rangeClosed(1, 100)
		.mapToObj(n ->
					fizzbuzz[n % 15].apply(
						buzz[n % 5].apply(
							fizz[n % 3].apply(String.valueOf(n))
						)
					)
				 )
		.forEach(System.out::println);
	}
}

